<?php

namespace App;

define("PER_PAGE", 5);

class Flickr
{

    /**
     * Get total count of photos from cached request
     *
     * @param $string
     * @return int
     */
    public function getTotalCount($string){
        preg_match('/(total=")([\d]+)(")/i',$string,$p);

        $totalCount = (int)$p[2];

        return $totalCount;
    }

    /**
     *  Search images in Flickr based on keyword and page
     *
     * @param $keyword
     * @param $page
     * @return array photos and pages count
     */
    public function getImages($keyword, $page){

        $api = new \Phlickr_Api(config('services.flickr.key'), config('services.flickr.secret'));
        $request = $api->createRequest(
            'flickr.photos.search',
            [
                'text'     => $keyword,
                'tag_mode' => 'all',
                'user_id'  => '',
            ]
        );

        $photoList = new \Phlickr_PhotoList($request, PER_PAGE);
        $photoList->setPage($page);
        $photos = $photoList->getPhotos();

        $cachedRequest = $request->getApi()->getCache()->get($request);
        $totalCount = self::getTotalCount($cachedRequest);

        return ['photos' => $photos, 'pagination' => ['totalItems' => $totalCount, 'perPage' => PER_PAGE, 'currentPage' => $page]];
    }
}
