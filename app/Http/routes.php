<?php


Route::get('/', 'HomeController@index');
Route::get('/search', 'HomeController@search');
Route::get('/show', 'HomeController@show');