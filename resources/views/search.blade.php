@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-4 pull-right show-search">
                    <form action="/search" method="GET">
                        <div class="input-group stylish-input-group">
                            <input type="text" class="form-control my-search" name="keyword" placeholder="Search" >
                        <span class="input-group-addon">
                            <button type="submit">
                                <span class="glyphicon glyphicon-search"></span>
                            </button>
                        </span>
                        </div>
                    </form>
            </div>
            </div>
            <div class="row">

            <div class="col-md-1"></div>
            @foreach($photos as $photo)
                <div class="col-md-2">
                    <a href="/show?url={{ $photo->buildImgUrl($photo::SIZE_ORIGINAL) }}" target="_blank"><img src="{{ $photo->buildImgUrl() }}" alt="{{ $photo->getTitle() }}" class="img-responsive"></a>
                </div>
            @endforeach
        </div>
    </div>

    <div class="text-center">
        @include('sub.paginator')
    </div>
@endsection
